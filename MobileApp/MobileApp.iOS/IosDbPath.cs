﻿using System;
using System.IO;
using MobileApp.iOS;
using Xamarin.Forms;

[assembly: Dependency(typeof(IosDbPath))]
namespace MobileApp.iOS
{
    public class IosDbPath : IPath
    {
        public string GetDatabasePath(string sqliteFilename)
        {
            return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "..", "Library", sqliteFilename);
        }
    }
}
