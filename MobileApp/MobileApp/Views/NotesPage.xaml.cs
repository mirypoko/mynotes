﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using MobileApp.Models;
using MobileApp.ViewModels;
using MobileApp.Services;
using System.Linq;

namespace MobileApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NotesPage : ContentPage
    {
        readonly NotesViewModel _viewModel;

        public NotesPage()
        {
            InitializeComponent();

            BindingContext = _viewModel = new NotesViewModel();
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            if (!(args.SelectedItem is NoteViewModel noteViewModel))
                return;

            await Navigation.PushAsync(new NoteDetailPage(noteViewModel));

            ItemsListView.SelectedItem = null;
        }

        async void AddItem_Clicked(object sender, EventArgs e)
        {
            if (_viewModel.IsBusy)
            {
                return;
            }
            _viewModel.IsBusy = true;
            await Navigation.PushModalAsync(new NavigationPage(new NewNotePage()));
            _viewModel.IsBusy = false;
        }

        async void OnCollorClicked(object sender, EventArgs e)
        {
            var collorsNames = ColorsService.Collors.Select(_ => _.Name).ToList();
            collorsNames.Insert(0, "All");
            var action = await DisplayActionSheet("Choose color", "Сancel", "Select", collorsNames.ToArray());
            if (action == "All")
            {
                _viewModel.FilterColor = null;
            }
            else
            {
                _viewModel.FilterColor = ColorsService.Collors.Single(_ => _.Name == action);

            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (_viewModel.ObservableNoteViewModels.Count == 0)
                _viewModel.LoadItemsCommand.Execute(null);
        }
    }
}