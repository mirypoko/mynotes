﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using MobileApp.Models;
using MobileApp.ViewModels;
using MobileApp.Extension;
using MobileApp.Services;
using System.Linq;

namespace MobileApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewNotePage : ContentPage
    {
        public NoteViewModel _viewModel { get; set; }

        public NewNotePage()
        {
            InitializeComponent();

            _viewModel = new NoteViewModel(new Note()
            {
                Theme = "My new note",
                Text = "Text of my new note",
                Color = ColorsService.Collors.Single(_ => _.Name == "Gray")
            });

            BindingContext = _viewModel;
        }

        async void OnCollorClicked(object sender, EventArgs e)
        {
            var collorsNames = ColorsService.Collors.Select(_ => _.Name).ToArray();
            var action = await DisplayActionSheet("Choose color", "Сancel", "Select", collorsNames);
            if (action != "Сancel")
            {
                _viewModel.Color = ColorsService.Collors.Single(_ => _.Name == action);
            }
        }

        async void Save_Clicked(object sender, EventArgs e)
        {
            if (!_viewModel.IsValid)
            {
                await DisplayAlert("Error", "Pleas, fill all felds", "Ok");
                return;
            }
            await _viewModel.SaveAsync();
            MessagingCenter.Send(this, "AddItem", _viewModel);
            await Navigation.PopModalAsync();
        }

        async void Cancel_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }
    }
}