﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using MobileApp.Models;
using MobileApp.ViewModels;
using MobileApp.Services;
using System.Linq;

namespace MobileApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NoteDetailPage : ContentPage
    {
        readonly NoteViewModel _viewModel;

        public NoteDetailPage(NoteViewModel viewModel)
        {
            InitializeComponent();
            BindingContext = _viewModel = viewModel;
            //TODO: To find another solution.
            _viewModel.Color = _viewModel.Color;
        }

        async void Remove_Clicked(object sender, EventArgs e)
        {
            var result = await DisplayAlert("Confirm", "Delete this note?", "Yes", "No");
            if (result)
            {
                MessagingCenter.Send(this, "RemoveItem", _viewModel);
                await _viewModel.RemoveAsync();
                await Navigation.PopAsync();
            }
        }

        async void OnCollorClicked(object sender, EventArgs e)
        {
            var collorsNames = ColorsService.Collors.Select(_ => _.Name).ToArray();
            var action = await DisplayActionSheet("Choose color", "Сancel", "Select", collorsNames);
            if (action != "Сancel")
            {
                _viewModel.Color = ColorsService.Collors.Single(_ => _.Name == action);
            }
        }

        async void Save_Clicked(object sender, EventArgs e)
        {
            if (!_viewModel.IsValid)
            {
                await DisplayAlert("Error", "Pleas, fill all felds", "Ok");
                return;
            }
            await _viewModel.SaveAsync();
            MessagingCenter.Send(this, "UpdateItem", _viewModel);
            await Navigation.PopAsync();
        }
    }
}