﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MobileApp.Models;
using MobileApp.Services;
using Xamarin.Forms;

namespace MobileApp.ViewModels
{
    public class NoteViewModel : BaseViewModel
    {
        public Note Note { get; private set; }

        public Command SaveCommand { get; set; }

        public Command RemoveCommand { get; set; }

        private readonly NotesService _notesService;

        public NoteViewModel(Note note, NotesService notesService = null)
        {
            Title = note.Theme;
            Note = note;
            if (notesService == null)
            {
                notesService = new NotesService();
            }
            _notesService = notesService;

            SaveCommand = new Command(async () => await SaveAsync());
            RemoveCommand = new Command(async () => await RemoveAsync());
        }

        public MobileApp.Models.Color Color
        {
            get
            {
                return ColorsService.Collors.Single(_ => _.Id == Note.ColorId);
            }
            set
            {
                if (Note.Color != value)
                {
                    Note.Color = value;
                    Note.ColorId = value.Id;
                    OnPropertyChanged(nameof(Note));
                }
            }
        }

        public Task RemoveAsync()
        {
            return _notesService.DeleteAsync(Note);
        }

        public Task SaveAsync()
        {
            Note.ColorId = Note.Color.Id;
            Note.Color = null;
            if (Note.Id == 0)
            {
                return _notesService.CreateAsync(Note);
            }
            return _notesService.UpdateAsync(Note);
        }

        public bool IsValid
        {
            get
            {
                return !string.IsNullOrWhiteSpace(Theme) && !string.IsNullOrWhiteSpace(Text);
            }
        }

        public string Text
        {
            get
            {
                return Note.Text;
            }
            set
            {
                Note.Theme = value;
            }
        }

        public string DateTimeUpdated
        {
            get
            {
                return Note.DateTimeUpdated.ToLocalTime().ToString();
            }
        }

        public string Theme
        {
            get
            {
                return Note.Theme;
            }
            set
            {
                Note.Theme = value;
            }
        }
    }
}
