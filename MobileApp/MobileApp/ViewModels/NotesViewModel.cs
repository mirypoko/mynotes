﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using MobileApp.Extension;
using MobileApp.Services;
using MobileApp.Views;
using Xamarin.Forms;

namespace MobileApp.ViewModels
{
    public class NotesViewModel : BaseViewModel
    {
        public List<NoteViewModel> NoteViewModels { get; private set; }

        public ObservableCollection<NoteViewModel> ObservableNoteViewModels { get; set; }

        public Command LoadItemsCommand { get; set; }

        private readonly Models.Color _defaultFilterCollor =
            new Models.Color
            {
                Name = "All colors",
                Hex = Color.LightGray.GetHexString()
            };

        readonly NotesService _notesService;

        public NotesViewModel()
        {
            _notesService = new NotesService();
            _filterColor = _defaultFilterCollor;
            Title = "Notes";
            ObservableNoteViewModels = new ObservableCollection<NoteViewModel>();
            NoteViewModels = new List<NoteViewModel>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommand());

            MessagingCenter.Subscribe<NoteDetailPage, NoteViewModel>(this, "RemoveItem", (obj, noteViewModel) =>
            {
                RemoveNoteFromCollection(noteViewModel);
            });

            MessagingCenter.Subscribe<NewNotePage, NoteViewModel>(this, "AddItem", (obj, noteViewModel) =>
            {
                InsertNoteToStartOfCollection(noteViewModel);
            });

            MessagingCenter.Subscribe<NoteDetailPage, NoteViewModel>(this, "UpdateItem", (obj, noteViewModel) =>
            {
                RemoveNoteAndInsertToStart(noteViewModel);
            });
        }

        private void RemoveNoteAndInsertToStart(NoteViewModel noteViewModel)
        {
            var noteObjectWithNewLink = new NoteViewModel(noteViewModel.Note, _notesService);
            RemoveNoteFromCollection(noteViewModel);
            InsertNoteToStartOfCollection(noteObjectWithNewLink);
        }

        private void InsertNoteToStartOfCollection(NoteViewModel noteViewModel)
        {
            ObservableNoteViewModels.Insert(0, noteViewModel);
            NoteViewModels.Insert(0, noteViewModel);
        }

        private void RemoveNoteFromCollection(NoteViewModel noteViewModel)
        {
            ObservableNoteViewModels.Remove(noteViewModel);
            NoteViewModels.Remove(noteViewModel);
        }

        private MobileApp.Models.Color _filterColor;
        public MobileApp.Models.Color FilterColor
        {
            get
            {
                return _filterColor;
            }
            set
            {
                if (_filterColor != value)
                {
                    List<NoteViewModel> noteViewModelsToShow;
                    ObservableNoteViewModels.Clear();
                    if (value == null)
                    {
                        _filterColor = _defaultFilterCollor;
                        noteViewModelsToShow = NoteViewModels.ToList();
                    }
                    else
                    {
                        _filterColor = value;
                        noteViewModelsToShow = NoteViewModels
                            .Where(_ => _.Color.Id == _filterColor.Id)
                            .ToList();
                    }

                    foreach (var viewModel in noteViewModelsToShow)
                    {
                        ObservableNoteViewModels.Add(viewModel);
                    }

                    OnPropertyChanged(nameof(FilterColor));
                }
            }
        }

        private string _search;
        public string Search
        {
            set
            {
                var filter = value.Trim().ToUpper();
                if (_search != filter)
                {
                    _search = filter;
                    ObservableNoteViewModels.Clear();
                    List<NoteViewModel> noteViewModelsToShow;
                    if (string.IsNullOrWhiteSpace(filter))
                    {
                        noteViewModelsToShow = NoteViewModels.ToList();
                    }
                    else
                    {
                        noteViewModelsToShow = NoteViewModels
                                  .Where(_ => _.Theme.ToUpper().Contains(filter) ||
                                              _.Text.ToUpper().Contains(value))
                                  .ToList();
                    }
                    foreach (var viewModel in noteViewModelsToShow)
                    {
                        ObservableNoteViewModels.Add(viewModel);
                    }
                }
            }
        }

        async Task ExecuteLoadItemsCommand()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                ObservableNoteViewModels.Clear();
                var notes = (await _notesService.GetListAsync())
                    .OrderByDescending(_ => _.DateTimeUpdated).ToList();

                foreach (var note in notes)
                {
                    var noteViewModel = new NoteViewModel(note, _notesService);
                    ObservableNoteViewModels.Add(noteViewModel);
                    NoteViewModels.Add(noteViewModel);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
