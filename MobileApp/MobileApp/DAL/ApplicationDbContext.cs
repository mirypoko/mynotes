﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using MobileApp.Models;
using Xamarin.Forms;

namespace MobileApp.DAL
{
    public class ApplicationDbContext : DbContext
    {
        private string _databasePath;

        public DbSet<Note> Notes { get; set; }

        public DbSet<MobileApp.Models.Color> Colors { get; set; }

        public ApplicationDbContext(string databasePath)
        {
            _databasePath = databasePath;
        }

        public ApplicationDbContext()
        {
            _databasePath = DependencyService.Get<IPath>().GetDatabasePath(App.DBFILENAME);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Filename={_databasePath}");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Note>()
                        .Property(_ => _.Id)
                        .ValueGeneratedOnAdd();

            modelBuilder.Entity<MobileApp.Models.Color>()
                        .Property(_ => _.Id)
                        .ValueGeneratedOnAdd();

            modelBuilder.Entity<MobileApp.Models.Color>()
                        .Property(_ => _.Name)
                        .IsUnicode();

            modelBuilder.Entity<MobileApp.Models.Color>()
                        .Property(_ => _.Hex)
                        .IsUnicode();
        }
    }
}
