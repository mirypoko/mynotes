﻿using System;
using MobileApp.Models.Base;

namespace MobileApp.Models
{
    public class Color : BaseEntity<int>
    {
        public string Name { get; set; }

        public string Hex { get; set; }
    }
}
