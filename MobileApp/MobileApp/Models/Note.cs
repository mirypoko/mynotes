﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using MobileApp.Models.Base;

namespace MobileApp.Models
{
    public class Note : BaseEntity<int>
    {
        [Required]
        public string Theme { get; set; }

        [Required]
        public string Text { get; set; }

        [Required]
        public DateTime DateTimeUpdated { get; set; }

        [Required]
        public int ColorId { get; set; }
        public Color Color { get; set; }
    }
}
