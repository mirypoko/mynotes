﻿using System.ComponentModel.DataAnnotations;

namespace MobileApp.Models.Base
{
    public abstract class BaseEntity<TKey> : IBaseEntity<TKey>
    {
        [Key] 
        public virtual TKey Id { get; set; }
    }
}