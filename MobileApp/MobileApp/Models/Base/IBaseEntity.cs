﻿namespace MobileApp.Models.Base
{
    public interface IBaseEntity<TKey>
    {
        TKey Id { get; set; }
    }
}