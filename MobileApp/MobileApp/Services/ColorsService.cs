﻿using System;
using System.Collections.Generic;
using System.Linq;
using MobileApp.DAL;
using MobileApp.Extension;
using Xamarin.Forms;

namespace MobileApp.Services
{
    public static class ColorsService
    {
        public static IReadOnlyList<MobileApp.Models.Color> Collors { get; private set; }

        public static void LoadCollors()
        {
            if (Collors == null)
            {
                using (var db = new ApplicationDbContext())
                {
                    var colorsFromDb = db.Colors.ToList();

                    var colors = new List<MobileApp.Models.Color>()
                    {
                        new MobileApp.Models.Color()
                        {
                            Name = "Gray",
                            Hex = Color.Gray.GetHexString()
                        },
                        new MobileApp.Models.Color()
                        {
                            Name = "Red",
                            Hex = Color.Red.GetHexString()
                        },
                        new MobileApp.Models.Color()
                        {
                            Name = "Indigo",
                            Hex = Color.Indigo.GetHexString()
                        },
                        new MobileApp.Models.Color()
                        {
                            Name = "Purple",
                            Hex = Color.Purple.GetHexString()
                        },
                        new MobileApp.Models.Color()
                        {
                            Name = "Green",
                            Hex = Color.Green.GetHexString()
                        },
                        new MobileApp.Models.Color()
                        {
                            Name = "Black",
                            Hex = Color.Black.GetHexString()
                        }
                    };

                    foreach (var color in colors)
                    {
                        if (colorsFromDb.All(_ => _.Name != color.Name))
                        {
                            db.Colors.Add(color);
                        }
                    }

                    db.SaveChanges();

                    Collors = db.Colors.ToList();
                }
            }
        }
    }
}
