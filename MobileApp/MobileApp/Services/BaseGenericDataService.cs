﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MobileApp.DAL;
using MobileApp.Models;
using MobileApp.Models.Base;

namespace Services.BaseServices
{
    public abstract class BaseGenericDataService<TKey, TEntity> where TEntity : class, IBaseEntity<TKey>
    {
        public virtual Task<TEntity> GetByIdOrDefaultAsync(TKey id)
        {
            using (var context = new ApplicationDbContext())
            {
                return context.Set<TEntity>().FindAsync(id);
            }
        }

        public virtual TEntity GetByIdOrDefault(TKey id)
        {
            using (var context = new ApplicationDbContext())
            {
                return context.Set<TEntity>().Find(id);
            }
        }

        public Task<List<TEntity>> GetListAsync()
        {
            using (var context = new ApplicationDbContext())
            {
                return context.Set<TEntity>().OrderBy(_ => _.Id).ToListAsync();
            }
        }

        public List<TEntity> GetList()
        {
            using (var context = new ApplicationDbContext())
            {
                return context.Set<TEntity>().OrderBy(_ => _.Id).ToList();
            }
        }

        public Task<List<TEntity>> GetListAsync(int count)
        {
            using (var context = new ApplicationDbContext())
            {
                return context.Set<TEntity>().OrderBy(_ => _.Id).Take(count).ToListAsync();
            }
        }

        public List<TEntity> GetList(int count)
        {
            using (var context = new ApplicationDbContext())
            {
                return context.Set<TEntity>().OrderBy(_ => _.Id).Take(count).ToList();
            }
        }

        public Task<List<TEntity>> GetListAsync(int count, int offset)
        {
            using (var context = new ApplicationDbContext())
            {
                return context.Set<TEntity>().OrderBy(_ => _.Id).Skip(offset).Take(count).ToListAsync();
            }
        }

        public List<TEntity> GetList(int count, int offset)
        {
            using (var context = new ApplicationDbContext())
            {
                return context.Set<TEntity>().OrderBy(_ => _.Id).Skip(offset).Take(count).ToList();
            }
        }

        public Task<List<TEntity>> GetListAsync(int? count, int? offset)
        {
            using (var context = new ApplicationDbContext())
            {
                return context.Set<TEntity>().OrderBy(_ => _.Id).Skip(offset.GetValueOrDefault()).Take(count.GetValueOrDefault())
                .ToListAsync();
            }
        }

        public List<TEntity> GetList(int? count, int? offset)
        {
            using (var context = new ApplicationDbContext())
            {
                return context.Set<TEntity>().OrderBy(_ => _.Id).Skip(offset.GetValueOrDefault()).Take(count.GetValueOrDefault())
                .ToList();
            }
        }

        public virtual Task<int> CountAsync()
        {
            using (var context = new ApplicationDbContext())
            {
                return context.Set<TEntity>().CountAsync();
            }
        }

        public virtual int Count()
        {
            using (var context = new ApplicationDbContext())
            {
                return context.Set<TEntity>().Count();
            }
        }

        public virtual async Task<ServiceResult> CreateAsync(TEntity entity)
        {
            using (var context = new ApplicationDbContext())
            {
                await context.Set<TEntity>().AddAsync(entity);
                await context.SaveChangesAsync();
            }
            return new ServiceResult(true, ServiceResult.ServiceResultType.Created, entity);
        }

        public virtual ServiceResult Create(TEntity entity)
        {
            using (var context = new ApplicationDbContext())
            {
                context.Set<TEntity>().Add(entity);
                context.SaveChanges();
            }
            return new ServiceResult(true, ServiceResult.ServiceResultType.Created, entity);
        }

        public virtual async Task<ServiceResult> DeleteAsync(TEntity entity)
        {
            using (var context = new ApplicationDbContext())
            {
                context.Set<TEntity>().Remove(entity);
                await context.SaveChangesAsync();
            }
            return new ServiceResult(true);
        }

        public virtual ServiceResult Delete(TEntity entity)
        {
            using (var context = new ApplicationDbContext())
            {
                context.Set<TEntity>().Remove(entity);
                context.SaveChanges();
            }
            return new ServiceResult(true);
        }

        public async Task<ServiceResult> DeleteAsync(TKey id)
        {
            using (var context = new ApplicationDbContext())
            {
                var entity = context.Set<TEntity>().Find(id);
                if (entity == null)
                {
                    return new ServiceResult(false, ServiceResult.ServiceResultType.NotFound);
                }
                context.Set<TEntity>().Remove(entity);
                await context.SaveChangesAsync();
            }
            return new ServiceResult(true);
        }

        public ServiceResult Delete(TKey id)
        {
            using (var context = new ApplicationDbContext())
            {
                var entity = context.Set<TEntity>().Find(id);
                if (entity == null)
                {
                    return new ServiceResult(false, ServiceResult.ServiceResultType.NotFound);
                }
                context.Set<TEntity>().Remove(entity);
                context.SaveChangesAsync();
            }
            return new ServiceResult(true);
        }

        public virtual async Task<ServiceResult> UpdateAsync(TEntity entity)
        {
            using (var context = new ApplicationDbContext())
            {
                context.Set<TEntity>().Update(entity);
                await context.SaveChangesAsync();
            }
            return new ServiceResult(true, ServiceResult.ServiceResultType.Updated, entity);
        }

        public virtual ServiceResult Update(TEntity entity)
        {
            using (var context = new ApplicationDbContext())
            {
                context.Set<TEntity>().Update(entity);
                context.SaveChanges();
            }
            return new ServiceResult(true, ServiceResult.ServiceResultType.Updated, entity);
        }
    }
}