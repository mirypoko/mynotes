﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MobileApp.DAL;
using MobileApp.Models;
using Services.BaseServices;

namespace MobileApp.Services
{
    public class NotesService : BaseGenericDataService<int, Note>
    {
        public NotesService()
        {

        }

        public List<Note> SearchNote(string searchString)
        {
            using (var context = new ApplicationDbContext())
            {
                return context.Notes
                              .Where(_ => _.Text.Contains(searchString) ||
                                     _.Theme.Contains(searchString))
                              .OrderBy(_ => _.Id)
                              .ToList();
            }

        }

        public override Task<ServiceResult> CreateAsync(Note entity)
        {
            entity.DateTimeUpdated = DateTime.UtcNow;
            return base.CreateAsync(entity);
        }

        public override ServiceResult Create(Note entity)
        {
            entity.DateTimeUpdated = DateTime.UtcNow;
            return base.Create(entity);
        }

        public override Task<ServiceResult> UpdateAsync(Note entity)
        {
            entity.DateTimeUpdated = DateTime.UtcNow;
            return base.UpdateAsync(entity);
        }

        public override ServiceResult Update(Note entity)
        {
            entity.DateTimeUpdated = DateTime.UtcNow;
            return base.Update(entity);
        }

    }
}
