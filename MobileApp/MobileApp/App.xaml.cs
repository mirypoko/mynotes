﻿using System;
using System.Collections.Generic;
using System.Linq;
using MobileApp.DAL;
using MobileApp.Extension;
using MobileApp.Services;
using MobileApp.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace MobileApp
{
    public partial class App : Application
    {
        public const string DBFILENAME = "notes.db";

        public App()
        {
            InitializeComponent();
            InitializeDb();
            MainPage = new MainPage();
        }

        private void InitializeDb()
        {
            string dbPath = DependencyService.Get<IPath>().GetDatabasePath(DBFILENAME);
            SQLitePCL.Batteries.Init();

            using (var db = new ApplicationDbContext(dbPath))
            {
                //db.Database.EnsureDeleted();
                db.Database.EnsureCreated();
            }
            ColorsService.LoadCollors();

        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
