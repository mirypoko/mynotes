﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MobileApp
{
    public interface IPath
    {
        string GetDatabasePath(string filename);
    }
}
